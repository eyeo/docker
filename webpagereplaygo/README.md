# webpagereplaygo

This contains a docker image which runs [web page replay go](https://chromium.googlesource.com/catapult/+/HEAD/web_page_replay_go/README.md)
and can be used during performance tests to eliminate network/response variance. Its mainly aimed at use during CI and can be used as a gitlab [service](https://docs.gitlab.com/ee/ci/services/).

# Environment Variables

**WARNING:** Due to a gitlab [bug](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27992) variables **must** be set in the `.gitlab-ci.yml`, they cannot be manually overridden for service containers.

`OUTPUT_DIR` - Absolute path of directory in which to save output file (eg logs). It must exist and defaults to the predefined gitlab env var `$CI_PROJECT_DIR` (which is shared between the main build container and all services)

`WPR_FILE` - Path to file (relative to `$OUTPUT_DIR`) to your wpr archive. If the file exists then wpr will replay it; if it does not exist wpr will go into 'record' mode and create a new wpr archive. This should be listed as a job artifact to save recordings.


`WPR_CONTAINER` - name (or IP) of the container running WPR. Used when setting up the tunnels, defaults to `wpr` and should match the gitlab service name (set via the alias).

`SKIP_IPTABLES_RULES` - Set to any string to skip changing iptables rules on the device. The iptables rules are responsible for redirecting outbound traffic to the container running the tests. Setting this variable allows for that to be skipped, this is useful in projects such as chromium which can use the `--host-resolver-rules` argument to redirect traffic instead.

**Note:** Although not an environment variable used by the container the [`GIT_CLEAN_FLAGS`](https://docs.gitlab.com/ee/ci/large_repositories/#git-clean-flags) variable does need to be set. This is to prevent gitlab removing the files we copy to `$CI_PROJECT_DIR` before the job starts.


The [`FF_NETWORK_PER_BUILD`](https://docs.gitlab.com/runner/executors/docker.html#network-per-build) feature flag also needs to be enabled via an environment variable. This allows the containers to properly communicate by name.

```yaml
GIT_CLEAN_FLAGS: "-ffdx -e wpr_scripts/"
FF_NETWORK_PER_BUILD: 1
```


# Usage in CI

Firstly declare the image as a `service` container in your job, remember the `alias` needs to match the `WPR_CONTAINER` environment variable:

```yaml
my_wpr_job:
  services:
    - name: "registry.gitlab.com/eyeo/docker/webpagereplaygo:21.06"
      alias: wpr
```

Before running any tests in your CI job you need to setup your device to use the wpr service, a convenience script, [`enable_wpr_on_device.sh`](wpr_scripts/enable_wpr_on_device.sh), is included in this repo and is copied to `$CI_PROJECT_DIR/wpr_scripts/` for use in jobs. You should already have an adb connection to your device (which **must be rooted**) before calling the script.

Note: Due to the way gitlab works, the `wpr_scripts` folder may not be copied immediately to your workspace, you may want to wait until it appears, using something like:

```shell
while [ ! -d wpr_scripts ] && [ "${waittime:-0}" -lt 60 ]
do
    echo "Waiting for wpr_scripts directory to appear..."
    ((waittime=waittime+1))
    sleep 1
done
```

After all tests have finished you should also use the corresponding `disable_wpr_on_device.sh` to stop forwarding traffic from the device to wpr, this should be done as an `after_script` so is called even on failed jobs.

# Stopping WPR

If wpr is in 'record' mode it needs to exit cleanly to correctly write to its output file. From the shell running CI its not possible to directly send any kill signals to wpr. To make this possible another convenience [script](wpr_scripts/stop_wpr.sh) is included which can be called to gracefully stop wpr.

# WPR Logs

The wpr container writes the logs from the main wpr process to `${CI_PROJECT_DIR}/wpr_scripts/wpr.log`, it's a good idea to expose this as a job artifact. Gitlab also checks the status of the exposed ports of services to determine container health, any errors in this health check will cause gitlab to write the service container stdout to the job output.


# Full, example job

Heres a full yaml snippet which should be enough to get something running:

```yaml
my_wpr_job:
  services:
    - name: "registry.gitlab.com/eyeo/docker/webpagereplaygo:21.06"
      alias: wpr
  variables:
    WPR_FILE: "recording.wpr"
    FF_NETWORK_PER_BUILD: 1
    GIT_CLEAN_FLAGS: "-ffdx -e wpr_scripts/"
  before_script:
    - adb devices
    - |
      while [ ! -d wpr_scripts ] && [ "${waittime:-0}" -lt 60 ]
      do
          echo "Waiting for wpr_scripts directory to appear..."
          ((waittime=waittime+1))
          sleep 1
      done
    - ./wpr_scripts/enable_wpr_on_device.sh
  script:
    - print "Running my awesome tests!"
    # run tests here
    - ./wpr_scripts/stop_wpr.sh
  after_script:
    - ./wpr_scripts/disable_wpr_on_device.sh
  artifacts:
    when: always
    paths:
      - $WPR_FILE
      - wpr_scripts/wpr.log
```
