# Copyright (c) 2021-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:18.04

# Install job requirements and create ~/.android/ ready for keys
RUN apt-get update \
&&  apt-get install -qy --no-install-recommends \
        adb \
        curl \
        jq \
        libatomic1 \
        python3-pip \
        python3-wheel \
        python3-setuptools \
        unzip \
        wget \
        xz-utils \
&&  mkdir --mode=750 ~/.android/ 

# Install golang (the default version in the repo is too old for wpr)
RUN wget --no-verbose https://dl.google.com/go/go1.16.3.linux-amd64.tar.gz \
&& echo "951a3c7c6ce4e56ad883f97d9db74d3d6d80d5fec77455c6ada6c1f7ac4776d2  go1.16.3.linux-amd64.tar.gz" | sha256sum --check \
&& tar -C /usr/local -xzf go*.tar.gz \
&& rm go*.tar.gz

# Install a newer version of node for lighthouse
RUN wget --no-verbose https://nodejs.org/dist/latest-v10.x/node-v10.24.1-linux-x64.tar.xz \
&& echo "a3b9b97c23bcdc64334be6b02422e9014f040d59dcf604563ffda48003419356  node-v10.24.1-linux-x64.tar.xz" | sha256sum --check \
&& tar -xf node-*.tar.xz \
&& rm node-*.tar.xz \
&& mv node-* /usr/local/node

# Update PATH to include new golang and node directories
ENV PATH "$PATH:/usr/local/node/bin:/usr/local/go/bin"
