#!/bin/bash

BL='\033[0;34m'
G='\033[0;32m'
RED='\033[0;31m'
YE='\033[1;33m'
NC='\033[0m' # No Color

emulator_name=${EMULATOR_NAME}

function check_hardware_acceleration() {
    if [[ "$HW_ACCEL_OVERRIDE" != "" ]]; then
        hw_accel_flag="$HW_ACCEL_OVERRIDE"
    else
        if [[ "$OSTYPE" == "darwin"* ]]; then
            # macOS-specific hardware acceleration check
            HW_ACCEL_SUPPORT=$(sysctl -a | grep -E -c '(vmx|svm)')
        else
            # generic Linux hardware acceleration check
            HW_ACCEL_SUPPORT=$(grep -E -c '(vmx|svm)' /proc/cpuinfo)
        fi

        if [[ $HW_ACCEL_SUPPORT == 0 ]]; then
            hw_accel_flag="-accel off"
        else
            hw_accel_flag="-accel on"
        fi
    fi

    echo "$hw_accel_flag"
}


hw_accel_flag=$(check_hardware_acceleration)

function launch_emulator () {
  adb devices | grep emulator | cut -f1 | xargs -I {} adb -s "{}" emu kill
  options="@${emulator_name} -no-window -no-snapshot -noaudio -no-boot-anim -cores 6 -memory 2048 ${hw_accel_flag} -camera-back none"

  if [[ -v EMULATOR_SNAPSHOT ]]; then
    options="${options} -snapshot ${EMULATOR_SNAPSHOT}"
  fi

  if [[ "$OSTYPE" == *linux* ]]; then
    echo "${OSTYPE}: emulator ${options} -gpu off"
    emulator $options -gpu off &
  fi
  if [[ "$OSTYPE" == *darwin* ]] || [[ "$OSTYPE" == *macos* ]]; then
    echo "${OSTYPE}: emulator ${options} -gpu swiftshader_indirect"
    emulator $options -gpu swiftshader_indirect &
  fi

  if [ $? -ne 0 ]; then
    echo "Error launching emulator"
    return 1
  fi
}


function check_emulator_status () {
  printf "${G}==> ${BL}Checking emulator booting up status 🧐${NC}\n"
  start_time=$(date +%s)
  spinner=( "⠹" "⠺" "⠼" "⠶" "⠦" "⠧" "⠇" "⠏" )
  i=0
  # Get the timeout value from the environment variable or use the default value of 300 seconds (5 minutes)
  timeout=${EMULATOR_TIMEOUT:-300}

  while true; do
    result=$(adb shell getprop sys.boot_completed 2>&1)

    if [ "$result" == "1" ]; then
      printf "\e[K${G}==> \u2713 Emulator boot completed: '$result'           ${NC}\n"
      adb devices -l
      adb shell input keyevent 82
      break
    elif [ "$result" == "" ]; then
      printf "${YE}==> Emulator is partially Booted! 😕 ${spinner[$i]} ${NC}\r"
    else
      printf "${RED}==> $result, please wait ${spinner[$i]} ${NC}\r"
      i=$(( (i+1) % 8 ))
    fi

    current_time=$(date +%s)
    elapsed_time=$((current_time - start_time))
    if [ $elapsed_time -gt $timeout ]; then
      printf "${RED}==> Timeout after ${timeout} seconds elapsed 🕛.. ${NC}\n"
      break
    fi
    sleep 4
  done
};


function disable_animation() {
  adb shell "settings put global window_animation_scale 0.0"
  adb shell "settings put global transition_animation_scale 0.0"
  adb shell "settings put global animator_duration_scale 0.0"
};

function hidden_policy() {
  adb shell "settings put global hidden_api_policy_pre_p_apps 1;settings put global hidden_api_policy_p_apps 1;settings put global hidden_api_policy 1"
};

function hidePopups() {
  EMU_BOOTED=0
  n=0
  first_launcher=1
  while [[ $EMU_BOOTED = 0 ]];do
      printf "${G}==> Test for current focus ${NC}\n"
      #        $ANDROID_HOME/platform-tools/adb shell dumpsys window
      CURRENT_FOCUS=`adb shell dumpsys window 2>/dev/null | grep -i mCurrentFocus`
      printf "${G}==> Current focus: ${BL} ${CURRENT_FOCUS} ${NC}\n"
      case $CURRENT_FOCUS in
      *"Launcher"*)
        if [[ $first_launcher == 1 ]]; then
          printf "${G}==> Launcher seems to be ready, wait 5 sec for another popup... ${NC}\n"
          sleep 5
          first_launcher=0
        else
          printf "${G}==> Launcher is ready, Android boot completed ${NC}\n"
          EMU_BOOTED=1
        fi
      ;;
      *"Not Responding: com.android.systemui"*)
        printf "${YE}==> Dismiss System UI isn't responding alert ${NC}\r"
        adb shell input keyevent KEYCODE_ENTER
        adb shell input keyevent KEYCODE_DPAD_DOWN
        adb shell input keyevent KEYCODE_ENTER
        first_launcher=1
      ;;
      *"Not Responding: com.google.android.gms"*)
        printf "${YE}==> Dismiss GMS isn't responding alert ${NC}\r"
        adb shell input keyevent KEYCODE_ENTER
        first_launcher=1
      ;;
      *"Not Responding: system"*)
        printf "${YE}==> Dismiss Process system isn't responding alert ${NC}\r"
        adb shell input keyevent KEYCODE_ENTER
        first_launcher=1
      ;;
      *"ConversationListActivity"*)
        printf "${YE}==> Close Messaging app ${NC}\r"
        adb shell input keyevent KEYCODE_ENTER
        first_launcher=1
      ;;
      *)
        n=$((n + 1))
        printf "${G}==> Android is not ready yet, wait 10 sec ($n)... ${NC}\n"
        sleep 10
        if [ $n -gt 60 ]; then
            printf "${RED}==> Android Emulator does not start in 10 minutes ${NC}\n"
            exit 2
        fi
      ;;
      esac
  done
  echo "Android Emulator started."
};

function waitIdle() {
  EMU_IDLE=0
  n=0
  while [[ $EMU_IDLE -lt 2 ]];do
    sleep 5
    n=$((n + 1))
    if [ $n -gt 60 ]; then
      printf "${RED}==> Android Emulator does not idle in 5 minutes ${NC}\n"
      exit 2
    fi
    # Extract the CPU usage summary line
    CPU_SUMMARY=$(adb shell top -n 1 -b | grep -E "^[0-9]+%cpu")

    # Extract the total CPU usage and idle percentage
    TOTAL_CPU=$(echo "$CPU_SUMMARY" | awk '{print $1}' | tr -d '%cpu')
    IDLE_CPU=$(echo "$CPU_SUMMARY" | awk '{print $5}' | tr -d '%idle')

    CPU_USAGE=$(echo "$TOTAL_CPU / $IDLE_CPU" | bc)
    if [[ $CPU_USAGE -lt 5 ]]; then
      EMU_IDLE=$((EMU_IDLE + 1))
      printf "${YE}==> Emulator is waiting: cpu=${CPU_USAGE}%% ${NC}\n"
    else
      EMU_IDLE=0
      printf "${RED}==> Emulator is working: cpu=${CPU_USAGE}%% ${NC}\n"
    fi
  done
  printf "${G}==> Emulator is idle ${NC}\n"

};


launch_emulator
sleep 2
check_emulator_status
sleep 1
disable_animation
sleep 1
hidden_policy
sleep 1
waitIdle
sleep 1
hidePopups
printf "${G}==> Emulator is ready ${NC}\n"