## pip-audit
This contains the Dockerfile and image for running pip-audit tool, pip-audit is a tool for scanning Python environments for packages with known vulnerabilities.

## Why in a docker file?
To standardize the scanning process and to make the invocation cleaner.

## How to run 
docker run --rm -v $(pwd):/src registry.gitlab.com/eyeo/docker/pip-audit -r /src/requirements.txt -f json > oast-pipa-results.json
