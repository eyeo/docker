#!/bin/bash
# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Script to install everything needed to build chromium on android, including
# items requiring sudo privileges.
# See https://www.chromium.org/developers/how-tos/android-build-instructions

mkdir /chromedriver 
cd /chromedriver/ 
echo '{' > /chromedriver/map.json 
for MAJOR_VERSION in $(seq 79 121)
do 
    if [ "$MAJOR_VERSION" -eq 82 ] ; then echo "Skipping non-existant chrome 82"; continue ; fi
    echo "Downloading chromedriver for chrome ${MAJOR_VERSION}" 
    if [ "$MAJOR_VERSION" -lt 115 ]
    then
        LATEST_VERSION=$(curl -f https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${MAJOR_VERSION}) 
        wget -q "https://chromedriver.storage.googleapis.com/${LATEST_VERSION}/chromedriver_linux64.zip"
    else 
        VERSIONS_LIST=$(curl -s "https://googlechromelabs.github.io/chrome-for-testing/latest-versions-per-milestone-with-downloads.json")
        DOWNLOAD_URL=$(echo "$VERSIONS_LIST" | jq -r --arg major "$MAJOR_VERSION" '.milestones[$major].downloads.chromedriver[] | select(.platform == "linux64") | .url')
        wget -q "$DOWNLOAD_URL" -O chromedriver_linux64.zip
    fi
    unzip chromedriver_linux64.zip 
    if [ -d "chromedriver-linux64/" ] 
    then 
        mv chromedriver-linux64/chromedriver /chromedriver/${MAJOR_VERSION}
        rm -rf chromedriver-linux64 
    else 
        rm -f LICENSE.chromedriver 
        mv chromedriver /chromedriver/${MAJOR_VERSION}
    fi
    rm -f chromedriver_linux64.zip
    echo """\"$(/chromedriver/${MAJOR_VERSION} --version | awk {'print $2'})\": \"${MAJOR_VERSION}\",""" >> /chromedriver/map.json
done
sed -i -e '$s/,//g' /chromedriver/map.json
echo '}' >> /chromedriver/map.json
