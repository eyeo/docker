#!/bin/bash
# Search for directories with a certain prefix and delete all but the newest one

prefix="$1"
search_path="$2"

# Find directories matching the prefix in the search path and retrieve their creation timestamps
directories=$(find "$search_path" -maxdepth 1 -type d -name "$prefix*" -exec stat -c "%Y %n" {} \; | sort -nr)

dir_count=$(echo "$directories" | grep -c "^")
if [[ $dir_count -eq 0 ]]; then
    echo "Nothing to clean."
    exit 0
elif [[ $dir_count -eq 1 ]]; then
    directory=$(echo "$directories" | awk '{print $2}')
    echo "cache found at $directory"
    echo "Nothing to clean."
    exit 0
fi

echo "All directories:"
while read -r timestamp directory; do
    echo "Timestamp: $timestamp, Directory: $directory"
done <<< "$directories"

# Delete all but the newest directory
count=0
while read -r timestamp directory; do
    if [ $count -ne 0 ]; then
        echo "Deleting directory: $directory"
        rm -rf "$directory"
    fi
    ((count++))
done <<< "$directories"