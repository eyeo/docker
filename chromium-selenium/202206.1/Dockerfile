FROM ubuntu:20.04

ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"

# Adjust env.
ENV DEBIAN_FRONTEND noninteractive
ENV TZ "${TIMEZONE}"

# Create our workdir
RUN mkdir "$WORK_DIR"

# Create non_root user to run tests
ARG USERNAME=seluser
ARG USER_UID=8877
ARG USER_GID=$USER_UID

# Create the user and give sudo permission to install packages when running tests
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD: /usr/bin/apt-get > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# Install Java JDK
RUN apt-get -y install software-properties-common
RUN add-apt-repository ppa:openjdk-r/ppa

# Requirements to run tests
ARG SYSTEM_REQUIREMENTS="\
    xvfb \
    unzip \
    wget \
    sudo \
    curl \
    openjdk-11-jre-headless \
    python3-pip \
    jq \
"
# Requirements to install Chromium
ARG CHROMIUM_REQUIREMENTS="\
    fonts-liberation \
    libasound2  \
    libatk1.0-0 \
    libc6 \
    libcairo2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libfontconfig1 \
    libgcc1 \
    libgconf-2-4 \
    libgdk-pixbuf2.0-0 \
    libglib2.0-0 \
    libgbm1 \
    libgtk-3-0 \
    libnspr4 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libstdc++6 \
    libu2f-udev \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    libnss3 \
    xdg-utils \
"

# Add jdk repository and install all requirements
RUN apt-get -y install software-properties-common \
&& add-apt-repository ppa:openjdk-r/ppa \
&& apt-get update \
&& apt-get install -yq --no-install-recommends $SYSTEM_REQUIREMENTS $CHROMIUM_REQUIREMENTS \
&&  apt-get clean \
&&  rm -rf /var/lib/apt/lists/*

# Install python-gitlab
RUN pip3 install python-gitlab

# Start container as non_root user to run tests
USER $USERNAME
