# Textpattern

Textpattern is a free and open-source content management system for PHP and MySQL.

The Dockerfile is installing gcsfuse, which will mount the Google bucket to the container. The bucket is what holds the files of textpattern, since they change everyday.

## Google Cloud Storage FUSE

FUSE will help us mount a Google file storage to the container.

https://cloud.google.com/storage/docs/gcs-fuse

## Testing with docker-compose

On this docker configuration, you can also find a docker-compose file for local testing. The docker-compose will build the Dockerfile and bring 2 containers:

 - 1 container with textpattern
 - 1 container with MySQL 5.7

Ports 80 and 3306 will be attached to your network. You may change it on the file, if you would prefer.

### Prerequisites

Before testing, you will need:

 - A service account credential key with the permissions role Storage Admin
 - A private bucket that you can upload textpattern files

Once you have the credential key, you can copy it to where docker-compose file is located and change the name to `key.json`. This file will be mounted to the container and used for authentication.

### Commands

To bring up the containers:
`$ docker-compose up --detach --build`

To stop containers:
`$ docker-compose down`
